(function($) {
  // Paypal Button.
  if($('.page-node-type-tour').length) {
    $(window).on('load', function() {
      $('#at4-scc').before($('<a role="button" tabindex="1" class="at-share-btn at-svc-paypal"><span class="at4-visually-hidden">Pagar con PayPal</span><span class="at-icon-wrapper" style="background: rgb(238,247,236)"><img src="/themes/custom/magico/images/ico-paypal.png" alt="PayPal" /></span></a>'));

      $('.at-svc-paypal').click(function(e) {
        e.preventDefault();
        $('#PayPalModal').modal('show');
        return false;
      })
    });  
  }

  // Fixed Menu.
  if (window.matchMedia('(min-width: 768px)').matches) {
    var menu = $('.main-nav');
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 85) {
            menu.addClass('fixed');
        } else {
            menu.removeClass('fixed');
        }
    });  
  }

  // Toogle Menu.
  $('.menu-resp .toggle button').click(function() {
    $(this).find('img').toggle();
    $('.main-nav').toggle();
  });

  // Responsive Menu.
  if (window.matchMedia('(max-width: 768px)').matches) {
    $('.main-nav > ul > li').on('click touch', function(e) {
      $(this).toggleClass('open');
      e.preventDefault();
    });

    $('.main-nav .child li a').on('click touch', function(e) {
      e.stopPropagation();
    });

    $('.main-nav > ul > li > a').on('click touch', function(e) {
      e.stopPropagation();
    });
  }

  // Blank
  $('.first ul li a, .sidebar ul li a')
    .attr('target', '_blank');

  /*if(typeof Tawk_API !== 'undefined'){
    Tawk_API.onLoad = function(){
      window.setInterval(function() {
        $('#tawkchat-container').css('display', 'none');  
      }, 500);
      
      console.log('oculto');
    }
  }

  $('.wamobile.otro a').click(function(e) {
      Tawk_API.maximize();
      return false;
  });*/


  
})(jQuery);
